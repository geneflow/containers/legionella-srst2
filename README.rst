Docker Container for Legionella SRST2
=====================================

This Dockerfile builds a container with Python 2.7.18, SciPy, Bowtie2-2.2.4 and SAMTools 0.1.18.

Build and push using the following commands (adjust tags):

.. code-block:: bash

    docker build . -t legionella-srst2:python2.7.18--scipy--bowtie2.2.4--samtools0.1.18
    docker tag legionella-srst2:python2.7.18--scipy--bowtie2.2.4--samtools0.1.18 geneflow/legionella-srst2:python2.7.18--scipy--bowtie2.2.4--samtools0.1.18
    docker push geneflow/legionella-srst2:python2.7.18--scipy--bowtie2.2.4--samtools0.1.18

