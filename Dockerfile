FROM python:2.7.18-slim-buster

RUN apt-get update \
    && apt-get -y upgrade \
    && apt-get install -y build-essential wget zlib1g-dev libncurses5-dev \
    && apt-get clean \
    && apt-get purge \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && cd /usr/src \
    && wget https://github.com/samtools/samtools/archive/0.1.18.tar.gz \
    && tar -xzf ./0.1.18.tar.gz \
    && rm ./0.1.18.tar.gz \
    && cd samtools-0.1.18 \
    && make \
    && cd /usr/src \
    && wget https://github.com/BenLangmead/bowtie2/archive/v2.2.4.tar.gz \
    && tar -xzf ./v2.2.4.tar.gz \
    && rm ./v2.2.4.tar.gz \
    && cd bowtie2-2.2.4 \
    && make \
    && pip install scipy

ENV PATH=/usr/src/samtools-0.1.18:/usr/src/bowtie2-2.2.4:${PATH}
